﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using Screen = GTA.UI.Screen;

namespace LastLocation
{
	public class Main : Script
	{
		enum SaveLocationError
		{
			None,
			NotApplicable,
			TooHigh
		}

		const string INI_SECTION_SETTINGS = "SETTINGS", INI_SECTION_LAST_POSITION = "LAST_POSITION";

		#region INI keys
		const string INI_KEY_AUTOMATIC = "AUTOMATIC",
					INI_KEY_MANUAL_SAVE_KEY = "MANUAL_SAVE_KEY",
					INI_KEY_MANUAL_REMOVE_SAVE_KEY = "MANUAL_REMOVE_SAVED_POSITION_KEY",
					INI_KEY_ALWAYS_KEEP_POSITION = "MANUAL_ALWAYS_KEEP_POSITION",
					INI_KEY_SAVE_PLAYER_MODEL = "SAVE_PLAYER_MODEL",
					INI_KEY_SAVE_POSITION_INTERVAL = "SAVE_POSITION_INTERVAL",
					INI_KEY_TIME_TO_FADE_IN = "TIME_TO_FADE_IN",
					INI_KEY_X = "X",
					INI_KEY_Y = "Y",
					INI_KEY_Z = "Z",
					INI_KEY_HEADING = "HEADING",
					INI_KEY_MODEL = "MODEL",
					INI_KEY_MONEY = "MONEY",
					INI_KEY_WEAPONS = "WEAPONS",
					INI_KEY_TIME = "TIME",
					INI_KEY_WEATHER = "WEATHER";
		#endregion

		readonly Vector3 ERROR_POS = new Vector3(-9999f, -9999f, -9999f);
		readonly WeaponHash[] ALL_WEAPONS = (WeaponHash[])Enum.GetValues(typeof(WeaponHash));

		readonly bool auto, alwaysKeepPosition, savePlayerModel;
		readonly Keys manualSaveKey, manualRemoveSaveKey;
		readonly int savePositionInterval, timeFade;

		Vector3 position;
		float heading;
		int modelHash;
		int money;
		string strWeapons;
		List<Tuple<WeaponHash, int>> weaponsAndAmmo = new List<Tuple<WeaponHash, int>>();
		TimeSpan time;
		Weather weather;

		bool startup = true, wasLoading = false;
		int originalModelHash = 0;
		DateTime nextSaveLocation = DateTime.UtcNow;

		public Main()
		{
			auto = Settings.GetValue(INI_SECTION_SETTINGS, INI_KEY_AUTOMATIC, true);
			manualSaveKey = Settings.GetValue(INI_SECTION_SETTINGS, INI_KEY_MANUAL_SAVE_KEY, Keys.End);
			manualRemoveSaveKey = Settings.GetValue(INI_SECTION_SETTINGS, INI_KEY_MANUAL_REMOVE_SAVE_KEY, Keys.None);
			alwaysKeepPosition = Settings.GetValue(INI_SECTION_SETTINGS, INI_KEY_ALWAYS_KEEP_POSITION, false);
			savePlayerModel = Settings.GetValue(INI_SECTION_SETTINGS, INI_KEY_SAVE_PLAYER_MODEL, true);
			savePositionInterval = Math.Max(100, Settings.GetValue(INI_SECTION_SETTINGS, INI_KEY_SAVE_POSITION_INTERVAL, 3000));
			timeFade = Math.Max(1, Math.Min(20000, Settings.GetValue(INI_SECTION_SETTINGS, INI_KEY_TIME_TO_FADE_IN, 3000)));

			try
			{
				float x = float.Parse(Settings.GetValue(INI_SECTION_LAST_POSITION, INI_KEY_X, ERROR_POS.X.ToString()).Replace(',', '.'), new CultureInfo("en-US"));
				float y = float.Parse(Settings.GetValue(INI_SECTION_LAST_POSITION, INI_KEY_Y, ERROR_POS.Y.ToString()).Replace(',', '.'), new CultureInfo("en-US"));
				float z = float.Parse(Settings.GetValue(INI_SECTION_LAST_POSITION, INI_KEY_Z, ERROR_POS.Z.ToString()).Replace(',', '.'), new CultureInfo("en-US"));
				position = new Vector3(x, y, z);
				heading = float.Parse(Settings.GetValue(INI_SECTION_LAST_POSITION, INI_KEY_HEADING, "0.0").Replace(',', '.'), new CultureInfo("en-US"));
			}
			catch
			{
				position = ERROR_POS;
				heading = 0f;
			}

			modelHash = Settings.GetValue(INI_SECTION_LAST_POSITION, INI_KEY_MODEL, 0);
			money = Settings.GetValue(INI_SECTION_LAST_POSITION, INI_KEY_MONEY, -1);
			strWeapons = Settings.GetValue(INI_SECTION_LAST_POSITION, INI_KEY_WEAPONS, "");

			TimeSpan tmpTime;
			if (TimeSpan.TryParse(Settings.GetValue(INI_SECTION_LAST_POSITION, INI_KEY_TIME, ""), out tmpTime))
			{
				time = tmpTime;
			}

			weather = Settings.GetValue(INI_SECTION_LAST_POSITION, INI_KEY_WEATHER, Weather.Unknown);

			ParseWeaponsString(strWeapons);

			Model mModel = modelHash;
			if (!mModel.IsValid || !mModel.IsInCdImage || !mModel.IsPed) modelHash = 0;

			Interval = 10;

			if (position == ERROR_POS)
			{
				startup = false; //don't set position if position setting isn't found

				if (auto) //if there isn't a last position to set then manual mode doesn't need a Tick
				{
					Tick += Main_Tick;
				}
			}
			else
			{
				Tick += Main_Tick; //Tick required for both auto and manual in case the player dies
			}

			if (!auto && (manualSaveKey != Keys.None || manualRemoveSaveKey != Keys.None)) KeyDown += Main_KeyDown;
		}

		private void Main_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == manualSaveKey)
			{
				var error = SaveLocation();

				if (error == SaveLocationError.None) Screen.ShowSubtitle("Saved");
				else if (error == SaveLocationError.TooHigh) Screen.ShowSubtitle("Too high off the ground to save");
			}
			else if (e.KeyCode == manualRemoveSaveKey)
			{
				DeleteLocation();

				Screen.ShowSubtitle("Removed saved location");
			}
		}

		private void Main_Tick(object sender, EventArgs e)
		{
			if (startup) //set position on startup
			{
				if (Game.IsLoading || Screen.IsFadedOut)
				{
					wasLoading = true;
					return;
				}

				Player plr = Game.Player;
				Ped plrPed = plr.Character;

				if (plrPed != null && plrPed.Exists() && plrPed.IsAlive)
				{
					if (wasLoading) //prevent changing position when reloading scripts
					{
						Tick -= Main_Tick; //don't tick while we're setting position

						Black.Render = true;

						Stopwatch stopwatch = new Stopwatch();
						stopwatch.Start();

						while (stopwatch.ElapsedMilliseconds < 10000)
						{
							if (plrPed.Position.DistanceTo(position) > 5f)
							{
								if (Function.Call<bool>(Hash.IS_PLAYER_SWITCH_IN_PROGRESS)) Function.Call(Hash.STOP_PLAYER_SWITCH);
								Function.Call(Hash.SET_CAM_ANIM_CURRENT_PHASE, World.RenderingCamera.Handle, 0.999f);
								plrPed.Task.ClearAllImmediately();

								Function.Call(Hash.LOAD_SCENE, position.X, position.Y, position.Z);

								Wait(100);

								plrPed.PositionNoOffset = position;

								int interior = Function.Call<int>(Hash.GET_INTERIOR_AT_COORDS, position.X, position.Y, position.Z);
								Function.Call(Hash.PIN_INTERIOR_IN_MEMORY, interior);
								Function.Call(Hash.SET_INTERIOR_ACTIVE, interior, true);
								Function.Call(Hash.REFRESH_INTERIOR, interior);

								Wait(500);
							}
							else break;
						}

						stopwatch.Reset();

						if (weather != Weather.Unknown) World.Weather = weather;
						if (time != default(TimeSpan)) World.CurrentTimeOfDay = time;

						if (savePlayerModel && modelHash != 0 && plrPed.Model.Hash != modelHash)
						{
							Wait(1000);

							originalModelHash = plrPed.Model.Hash;

							plr.ChangeModel(modelHash);

							Wait(25);

							plrPed = plr.Character;

							plrPed.IsCollisionEnabled = true;
							plrPed.PositionNoOffset = position;
							plrPed.IsVisible = true;
							plrPed.Style.SetDefaultClothes();
							plrPed.Task.ClearAllImmediately();
						}

						plr.CanControlCharacter = true;

						plrPed.Heading = heading;
						if (money != -1) plr.Money = money;
						GiveWeaponsAndAmmo(plrPed);

						FixCamera();

						Black.FadeIn(timeFade);

						if (!auto && !alwaysKeepPosition)
						{
							DeleteLocation();
						}

						if (auto) Tick += Main_Tick;
					}
					else if (!auto) Tick -= Main_Tick; //Tick not needed for manual
					
					startup = false;
					wasLoading = false;
				}
			}
			else
			{
				if (auto)
				{
					if (DateTime.UtcNow >= nextSaveLocation)
					{
						SaveLocation();
						nextSaveLocation = DateTime.UtcNow.AddMilliseconds(savePositionInterval);
					}
				}

				if (savePlayerModel && originalModelHash != 0 && Game.Player.Character.IsDead && Screen.IsFadingOut)
				{
					if (Game.Player.Character.Model.Hash != originalModelHash) Game.Player.ChangeModel(originalModelHash);
					originalModelHash = 0;
				}
			}
		}

		private void FixCamera()
		{
			//if camera is third person it sometimes takes ~8 seconds before the camera moves to the player's location. this seems to fix that problem
			if (Function.Call<int>(Hash.GET_FOLLOW_PED_CAM_VIEW_MODE) != 4) //!= firstPerson
			{
				Function.Call(Hash._RENDER_FIRST_PERSON_CAM, true, 0, 3, 0);
				Function.Call(Hash._RENDER_FIRST_PERSON_CAM, false, 0, 3, 0);
			}
		}

		private void SetSettingsValues(ScriptSettings ini)
		{
			ini.SetValue(INI_SECTION_SETTINGS, INI_KEY_AUTOMATIC, auto);
			ini.SetValue(INI_SECTION_SETTINGS, INI_KEY_MANUAL_SAVE_KEY, manualSaveKey);
			ini.SetValue(INI_SECTION_SETTINGS, INI_KEY_MANUAL_REMOVE_SAVE_KEY, manualRemoveSaveKey);
			ini.SetValue(INI_SECTION_SETTINGS, INI_KEY_ALWAYS_KEEP_POSITION, alwaysKeepPosition);
			ini.SetValue(INI_SECTION_SETTINGS, INI_KEY_SAVE_PLAYER_MODEL, savePlayerModel);
			ini.SetValue(INI_SECTION_SETTINGS, INI_KEY_SAVE_POSITION_INTERVAL, savePositionInterval);
			ini.SetValue(INI_SECTION_SETTINGS, INI_KEY_TIME_TO_FADE_IN, timeFade);
		}

		private void SetLocationValues(ScriptSettings ini, Vector3 position, float heading, int model, int money, string strWeapons, TimeSpan time, Weather weather)
		{
			ini.SetValue(INI_SECTION_LAST_POSITION, INI_KEY_X, position.X);
			ini.SetValue(INI_SECTION_LAST_POSITION, INI_KEY_Y, position.Y);
			ini.SetValue(INI_SECTION_LAST_POSITION, INI_KEY_Z, position.Z + 0.05f);
			ini.SetValue(INI_SECTION_LAST_POSITION, INI_KEY_HEADING, heading);
			ini.SetValue(INI_SECTION_LAST_POSITION, INI_KEY_MODEL, model);
			ini.SetValue(INI_SECTION_LAST_POSITION, INI_KEY_MONEY, money);
			ini.SetValue(INI_SECTION_LAST_POSITION, INI_KEY_WEAPONS, strWeapons);
			ini.SetValue(INI_SECTION_LAST_POSITION, INI_KEY_TIME, time);
			ini.SetValue(INI_SECTION_LAST_POSITION, INI_KEY_WEATHER, weather);
		}

		private void ParseWeaponsString(string strWeapons)
		{
			if (!string.IsNullOrWhiteSpace(strWeapons))
			{
				string[] strArrWeapons = strWeapons.Split(';');

				foreach (var strWeaponAndAmmo in strArrWeapons)
				{
					string[] strArrWeaponAndAmmo = strWeaponAndAmmo.Split(':');

					uint uWeapon;
					int ammo;

					if (uint.TryParse(strArrWeaponAndAmmo[0], out uWeapon) && int.TryParse(strArrWeaponAndAmmo[1], out ammo))
					{
						WeaponHash weapon = (WeaponHash)uWeapon;

						if (Array.IndexOf(ALL_WEAPONS, weapon) >= 0)
						{
							weaponsAndAmmo.Add(new Tuple<WeaponHash, int>(weapon, ammo));
						}
					}
				}
			}
		}

		private string CreateWeaponsString(Ped ped)
		{
			string strWeaponsAndAmmo = "";

			foreach (var weaponHash in ALL_WEAPONS)
			{
				if (ped.Weapons.HasWeapon(weaponHash))
				{
					int ammo = Function.Call<int>(Hash.GET_AMMO_IN_PED_WEAPON, ped, (uint)weaponHash);

					strWeaponsAndAmmo += $"{(uint)weaponHash}:{ammo};";
				}

				Yield();
			}

			if (strWeaponsAndAmmo != "") strWeaponsAndAmmo.Remove(strWeaponsAndAmmo.Length - 1, 1); //remove last ; char

			return strWeaponsAndAmmo;
		}

		private void GiveWeaponsAndAmmo(Ped ped)
		{
			ped.Weapons.RemoveAll();

			if (weaponsAndAmmo.Count > 0)
			{
				foreach (var weaponAndAmmo in weaponsAndAmmo)
				{
					ped.Weapons.Give(weaponAndAmmo.Item1, weaponAndAmmo.Item2, false, true);
				}
			}

			ped.Weapons.Select(WeaponHash.Unarmed, true);
		}

		private SaveLocationError SaveLocation()
		{
			Player plr = Game.Player;
			Ped plrPed = plr.Character;

			if (plrPed != null && plrPed.Exists() && plrPed.IsAlive && !plrPed.IsRagdoll)
			{
				Vehicle veh;

				if ((plrPed.IsInVehicle() && (veh = plrPed.CurrentVehicle).Exists() && veh.HeightAboveGround < 10f && veh.HeightAboveGround > 0f) || plrPed.HeightAboveGround < 5f && plrPed.HeightAboveGround > 0f)
				{
					if (!auto || plrPed.Position.DistanceTo(position) > 0.5f || Math.Abs(heading - plrPed.Heading) > 5 || plrPed.Model.Hash != modelHash || plr.Money != money) //only save if in manual mode or if the player's position or heading has changed
					{
						position = plrPed.Position;
						heading = plrPed.Heading;
						money = plr.Money;
						strWeapons = CreateWeaponsString(plrPed);
						time = World.CurrentTimeOfDay;
						weather = World.Weather;

						SetSettingsValues(Settings);
						SetLocationValues(Settings, position, heading, plrPed.Model.Hash, money, strWeapons, time, weather);

						Settings.Save();
					}
				}
				else return SaveLocationError.TooHigh;
			}
			else return SaveLocationError.NotApplicable;

			return SaveLocationError.None;
		}

		private void DeleteLocation()
		{
			string iniFilename = Path.ChangeExtension(Filename, "ini");

			if (File.Exists(iniFilename)) File.Delete(iniFilename);

			File.Create(iniFilename).Dispose();

			var ini = ScriptSettings.Load(iniFilename);

			SetSettingsValues(ini);
			ini.Save();
		}
	}
}
