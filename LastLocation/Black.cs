﻿using GTA;
using GTA.UI;
using System;
using System.Diagnostics;
using System.Drawing;
using Screen = GTA.UI.Screen;

namespace LastLocation
{
    class Black : Script
    {
        internal static bool Render = false;

        private static ContainerElement blackRect = new ContainerElement(new Point(0, 0), Screen.Resolution, Color.Black);

        public Black()
        {
            Interval = 0;
            Tick += Black_Tick;
        }

        private void Black_Tick(object sender, EventArgs e)
        {
            if (Render) blackRect.Draw();
        }

		public static void FadeIn(int time)
		{
			float alphaDivisor = time / 254f;

			Stopwatch stopwatch = new Stopwatch();

			stopwatch.Start();
			
			while (stopwatch.ElapsedMilliseconds < time)
			{
				blackRect.Color = Color.FromArgb((int)((time - stopwatch.ElapsedMilliseconds) / alphaDivisor), Color.Black);

				Wait(1);
			}

			Render = false;
		}
    }
}
